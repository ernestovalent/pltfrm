<?php

/** Cnfg.php
 *  Return a Global config data
 * Autor: Ernesto Valentin Caamal Peech
 * 2019/11/6
 */

return array(
  "DBHOST"  => "localhost",
  "DBNAME"  => "pltfrm",
  "DBUSER"  => "root",
  "DBPASSWRD"  => "",
  //  "DBDRIVER"  =>"mysql",
  //  "DBHOST"  =>"162.241.60.169:3306",
  //  "DBNAME"  =>"toursal1_dev",
  //  "DBUSER"  =>"toursal1_devuser",
  //  "DBPASSWRD"  =>"YE89QYidCu6Ybb",
  "WEBNAME"  => "Pltfrm",
  "WEBDESCRIPTION"  => "Site with content management system",
  "WEBAUTHOR"  => "Ernesto Valentin Caamal Peech",
  "WEBKEYWORKS"  => "Pltfrm, CMS, CRM, Web, Platfrm",
  "WEBHOSTING"  => "//localhost/pltfrm"
);
