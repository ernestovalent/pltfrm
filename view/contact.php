<!DOCTYPE html>
<html lang="Mx" dir="ltr" class="pltfrm">
  <head>
    <?php
    include "view/head/header.php";
    include "view/head/resource.php"; ?>
  </head>
  <body>
    <div class="container">
      <h1 class="center"><?php echo WEBNAME; ?></h1>
      <div class="row">
        <div class="col s12 m10 l6 offset-m1 offset-l3">
          <nav class="card row">
            <a class="col s4" href="index">Index</a>
            <a class="col s4"href="home">Home</a>
            <a class="col s4"href="contact">Forms</a>
          </nav>
        </div>
      </div>
      <div class="row card">
        <div class="col s12">
          <h2 class="center">Principal Forms</h2>
          <div class="row">
            <div class="col s12 m5 hide-s">
              <img class="responsive" src="view/img/forms.png" alt="formulario">
            </div>
            <div class="col s12 m7">
              <form action="" class="form container" id="addContact" method="post">
                <div class="row">
                  <div class="col s12 l6">
                    <label for="name">Name</label>
                    <input type="text" name="name" placeholder="Jonh" required>
                  </div>
                  <div class="col s12 l6">
                    <label for="lastname">Last Name</label>
                    <input type="text" name="lastname" placeholder="Smith">
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <label for="email">Email adress</label>
                    <input type="email" name="email" placeholder="jhon.smith@mail.com" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <label for="date">Date</label>
                    <input type="date" name="date" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <label for="subject">Subject</label>
                    <input type="text" name="subject" placeholder="Reservation Request" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <label for="message">Message</label>
                    <textarea name="message"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <button type="submit">Enviar correo</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <?php
          $reg = New Contact();
           ?>
        </div>
      </div>
    </div>

    <?php include "view/footer/resource.php"; ?>
    <script src="view/js/main.js"></script>
  </body>
</html>
