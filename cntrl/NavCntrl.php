<?php
/** Nav.php
 *  Is a Class that use for Navegation Model
 */
class NavCntrl{

  static function includeView($view){ //Get rute of view directory
    if($view == "index" ||
        $view == "home"){ //Views that not required database access
      $rsc = "view/".$view.".php";
    }elseif ($view == "contact") { //Views that required database access
      require_once "cntrl/ContactCntrl.php"; //Add Access to Data Base
      $rsc = "view/".$view.".php";
    }else {
      $rsc = "view/core/404.php";
    }
    return $rsc;
  }

}
 ?>
