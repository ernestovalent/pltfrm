<?php

/** ContactCntrl.php
 *  Class of example for functions to forms
 *  Autor: Ernesto Valentin Caamal Peech
 *  2019/11/10
 */
require_once "mdl/ContactMdl.php"; //Need for database function to Contact
require_once "core/Validate.php"; //Need for validate 
class Contact extends ContactModel
{


  function __construct()
  {
    $this->getPost();
  }

  public function getPost()
  {
    if (
      isset($_POST["name"]) &&
      isset($_POST["email"]) &&
      isset($_POST["date"]) &&
      isset($_POST["subject"])
    ) { //First validate inputs Required
      if (
        Validate::name($_POST["name"], 45) &&
        Validate::name($_POST["lastname"], 45) &&
        Validate::email($_POST["email"], 120) &&
        Validate::date($_POST["date"]) &&
        Validate::text($_POST["subject"], 120) &&
        Validate::text($_POST["message"], 1000)
      ) { //Second validate inputs content
        $data = array(
          "reference" => "Pltfrm",
          "name" => $_POST["name"],
          "lastname" => $_POST["lastname"],
          "interesting" => "Developers",
          "email" => $_POST["email"],
          "date" => $_POST["date"],
          "subject" => $_POST["subject"],
          "message" => $_POST["message"],
          "recipient" => "ernesto.valen.t@gmail.com",
          "reservationname" => "My Reservation Dev"
        ); //Create array with data
        $answer = new ContactModel(); //Init Model
        if ($answer->saveContact($data)) {
          $this->msg(true);
        } else {
          $this->msg(false);
        }
        $this->cleanFrm();
      } else {
        return false;
      }
    } else {
      $answer = new ContactModel();
    }
  }

  public function showPost()
  {
    var_dump($_POST);
  }

  private function msg($bool)
  {
    if ($bool) {
      echo "Enviado correctamente";
    } else {
      echo "No registrado correctamente";
    }
  }

  public function cleanFrm()
  {
    echo "<script>
              if (window.history.replaceState) {
                window.history.replaceState(null, null, window.location.href);
              }
            </script>";
  }
}
