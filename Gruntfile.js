module.exports = function(grunt) {
  grunt.initConfig({
    //Watch Contrib Task
    watch:{
      sass: {
        files: ['view/sass/*.scss'],
        tasks: ['sass'],
        options: {
          livereload: true
        }
      }
    },
    //Sass Task
    sass:{
      dev:{
        options: {
          style: 'expanded'
        },
        files:{
          'view/css/main.css':'view/sass/main.scss'
        }
      }
    },
    //BrowserSync Task
    browserSync: {
      dev: {
        bsFiles: {
          src: [
            'cntrl/*php',
            'core/*php',
            'mdl/*php',
            'view/*php',
            'view/core/*php',
            'view/css/main.css'
          ]
        },
        options: {
          watchTask: true,
          proxy: 'localhost/pltfrm'
        }
      }
    }
  });
  //load tasks
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');


  // define tasking on comands
  grunt.registerTask('default', ['browserSync','watch']); // grun sass compile Sass
  //grunt.registerTask('monitor', ['browserSync']); // grunt compile browserSync
};
